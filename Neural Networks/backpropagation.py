# -*- coding: utf-8 -*-
"""
This code shows an example of NN backpropagation. The data are 5000 instances of 20x20 pixels 
of handwritten digits in grayscale, along with the value of the digits (0-9), taken from the 
Machine Learning Stanford class of Andrew Ng. 
"""

import numpy as np  
from scipy.io import loadmat
from scipy.optimize import minimize 
from sklearn.preprocessing import OneHotEncoder 

def sigmoid(z):  
    return 1 / (1 + np.exp(-z))

def sigmoid_gradient(z):  
    return np.multiply(sigmoid(z), (1 - sigmoid(z)))

def forward_propagate(X, theta1, theta2):       # forward propagate function for a NN with one hidden layer for all data points at once
    m = X.shape[0]                              # number of data points
    a1 = np.insert(X, 0, values=np.ones(m), axis=1)     # insert bias nodes at input layer
    z2 = a1 * theta1.T                          # calculate weighted sum at the hidden layer input
    a2 = np.insert(sigmoid(z2), 0, values=np.ones(m), axis=1) # insert bias nodes and calculate sigmoid functions at hidden layer
    z3 = a2 * theta2.T                          # calculate weighted sum at the output layer input
    h = sigmoid(z3)                             # calculate sigmoid function at NN output
    return a1, z2, a2, z3, h

def backprop(params, input_size, hidden_size, num_labels, X, y, reg_term):  
    ##### this section is identical to the cost function logic we already saw #####
    m = X.shape[0]
    X = np.matrix(X)
    y = np.matrix(y)

    # reshape the parameter array into parameter matrices for each layer
    theta1 = np.matrix(np.reshape(params[:hidden_size * (input_size + 1)], (hidden_size, (input_size + 1))))
    theta2 = np.matrix(np.reshape(params[hidden_size * (input_size + 1):], (num_labels, (hidden_size + 1))))

    # run the feed-forward pass
    a1, z2, a2, z3, h = forward_propagate(X, theta1, theta2)

    # initializations
    J = 0
    delta1 = np.zeros(theta1.shape)  # (25, 401)
    delta2 = np.zeros(theta2.shape)  # (10, 26)

    # compute the cost
    for i in range(m):
        first_term = np.multiply(-y[i,:], np.log(h[i,:]))
        second_term = np.multiply((1 - y[i,:]), np.log(1 - h[i,:]))
        J += np.sum(first_term - second_term)

    J = J / m

    # add the cost regularization term
    J += (float(reg_term) / (2 * m)) * (np.sum(np.power(theta1[:,1:], 2)) + np.sum(np.power(theta2[:,1:], 2)))

    ##### end of cost function logic, below is the new part #####

    # perform backpropagation
    for t in range(m):
        a1t = a1[t,:]  # (1, 401)
        z2t = z2[t,:]  # (1, 25)
        a2t = a2[t,:]  # (1, 26)
        ht = h[t,:]  # (1, 10)
        yt = y[t,:]  # (1, 10)

        d3t = ht - yt  # (1, 10)

        z2t = np.insert(z2t, 0, values=np.ones(1))  # (1, 26)
        d2t = np.multiply((theta2.T * d3t.T).T, sigmoid_gradient(z2t))  # (1, 26)

        delta1 = delta1 + (d2t[:,1:]).T * a1t   # don't add the bias error in the hidden layer
        delta2 = delta2 + d3t.T * a2t           # there is no bias error in the input layer

    delta1 = delta1 / m
    delta2 = delta2 / m

    # add the gradient regularization term
    delta1[:,1:] = delta1[:,1:] + (theta1[:,1:] * reg_term) / m
    delta2[:,1:] = delta2[:,1:] + (theta2[:,1:] * reg_term) / m

    # unravel the gradient matrices into a single array
    grad = np.concatenate((np.ravel(delta1), np.ravel(delta2)))

    return J, grad                      # this backpropagation function returns the initial cost and the gradients (errors)

data = loadmat('ex3data1.mat')          # open test data - handwritten digits, 5000 images of 400 pixels each 
print "Data:\n"
print data 

X = data['X']  
y = data['y'] 
print X.shape
print y.shape

encoder = OneHotEncoder(sparse=False)   # transform the output into ten outputs for all possible classes (0-9)
y_onehot = encoder.fit_transform(y)  
print y_onehot.shape 

# initial setup
input_size = 400                        # number of input features, without the bias neuron
hidden_size = 25                        # number of neurons in hidden layer, without bias
num_labels = 10                         # number of neurons in output layer, same as number of classes
reg_term = 1                            # regularization term lambda

# randomly initialize a parameter array of the size of the full network's parameters, in range [-0.125, 0.125]
params = (np.random.random(size=hidden_size * (input_size + 1) + num_labels * (hidden_size + 1)) - 0.5) * 0.25

J, grad = backprop(params, input_size, hidden_size, num_labels, X, y_onehot, reg_term)
print "J: "
print J
print "grad"
print grad
print grad.shape 

# minimize the objective function in 250 iterations to obtain the theta parameter values,
# jac=True assumes that fun=backprop returns both the cost and the gradient, as it does
# more info at: https://docs.scipy.org/doc/scipy-0.18.1/reference/generated/scipy.optimize.minimize.html 
fmin = minimize(fun=backprop, x0=params, args=(input_size, hidden_size, num_labels, X, y_onehot, reg_term),  
                method='TNC', jac=True, options={'maxiter': 100})       # this could take a while...

print fmin

# Finally, do a forward propagation with the obtained optimal parameters and predict the outputs (y_pred)
X = np.matrix(X)  
theta1 = np.matrix(np.reshape(fmin.x[:hidden_size * (input_size + 1)], (hidden_size, (input_size + 1))))  
theta2 = np.matrix(np.reshape(fmin.x[hidden_size * (input_size + 1):], (num_labels, (hidden_size + 1))))

a1, z2, a2, z3, h = forward_propagate(X, theta1, theta2)  
y_pred = np.array(np.argmax(h, axis=1) + 1)     # y_pred contains the predicted digits
print y_pred

# Compute accuracy: 
counter = 0
for i in range(y_pred.shape[0]):
    if (y_pred[i,0] == y[i,0]):
        counter += 1
        
print "Accuracy: "
print counter*100.0/y_pred.shape[0] 
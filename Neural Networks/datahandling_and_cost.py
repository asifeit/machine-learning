# -*- coding: utf-8 -*-
"""
This code shows an example of data handling and cost function value calculation in a neural network.
The data are 5000 instances of 20x20 pixels of handwritten digits in grayscale, along with the value 
of the digits (0-9), taken from the Machine Learning Stanford class of Andrew Ng. 
"""

import numpy as np  
from scipy.io import loadmat
from sklearn.preprocessing import OneHotEncoder 

def sigmoid(z):  
    return 1 / (1 + np.exp(-z))

def forward_propagate(X, theta1, theta2):       # forward propagate function for a NN with one hidden layer for all data points at once
    m = X.shape[0]                              # number of data points
    a1 = np.insert(X, 0, values=np.ones(m), axis=1)     # insert bias nodes at input layer
    z2 = a1 * theta1.T                          # calculate weighted sum at the hidden layer input
    a2 = np.insert(sigmoid(z2), 0, values=np.ones(m), axis=1) # insert bias nodes and calculate sigmoid functions at hidden layer
    z3 = a2 * theta2.T                          # calculate weighted sum at the output layer input
    h = sigmoid(z3)                             # calculate sigmoid function at NN output
    return a1, z2, a2, z3, h

def cost(params, input_size, hidden_size, num_labels, X, y, reg_term):  
    m = X.shape[0]
    X = np.matrix(X)
    y = np.matrix(y)

    # reshape the parameter array into parameter matrices for each layer
    theta1 = np.matrix(np.reshape(params[:hidden_size * (input_size + 1)], (hidden_size, (input_size + 1))))
    theta2 = np.matrix(np.reshape(params[hidden_size * (input_size + 1):], (num_labels, (hidden_size + 1))))

    # run the feed-forward pass
    a1, z2, a2, z3, h = forward_propagate(X, theta1, theta2)

    # compute the cost
    J = 0
    for i in range(m):
        first_term = np.multiply(-y[i,:], np.log(h[i,:]))
        second_term = np.multiply((1 - y[i,:]), np.log(1 - h[i,:]))
        J += np.sum(first_term - second_term)

    J = J / m
    # add the regularization term
    J += (float(reg_term) / (2 * m)) * (np.sum(np.power(theta1[:,1:], 2)) + np.sum(np.power(theta2[:,1:], 2)))  

    return J

data = loadmat('ex3data1.mat')          # open test data - handwritten digits, 5000 images of 400 pixels each 
print "Data:\n"
print data 

X = data['X']  
y = data['y'] 
print X.shape
print y.shape

encoder = OneHotEncoder(sparse=False)   # transform the output into ten outputs for all possible classes (0-9)
y_onehot = encoder.fit_transform(y)  
print y_onehot.shape                    # compare y_onehot shape with y shape

# initial setup
input_size = 400                        # number of input features, without the bias neuron
hidden_size = 25                        # number of neurons in hidden layer, without bias
num_labels = 10                         # number of neurons in output layer, same as number of classes
reg_term = 1                            # regularization term Lambda

# randomly initialize a parameter array of the size of the full network's parameters, in range [-0.125, 0.125]
params = (np.random.random(size=hidden_size * (input_size + 1) + num_labels * (hidden_size + 1)) - 0.5) * 0.25
# calculate the cost J
print cost(params, input_size, hidden_size, num_labels, X, y_onehot, reg_term) 
# -*- coding: utf-8 -*-
"""
This code shows an example of the use of colaborative filtering for the purpose
of movie rating prediction. Given a matrix of movie ratings of previous users,
new user ratings for few movies are inserted and then colaborative filtering is
used to predict the top ten rated movies for that new user. This same code with 
minimal changes can also be used to calculate the accuracy of the algorithm, or 
to make rating predictions for old users, based on the changes in the features and
preferences parameters (these changes happen everytime a new user rating is inserted). 
"""

import numpy as np  
from scipy.io import loadmat 

# Cost function computation: 
def cost(params, Y, R, num_features, regularization):
    Y = np.matrix(Y)  # (no. movies, no. users)
    R = np.matrix(R)  # (no. movies, no. users)
    num_movies = Y.shape[0]
    num_users = Y.shape[1]
    
    # reshape the parameter array into parameter matrices
    X = np.matrix(np.reshape(params[:num_movies * num_features], (num_movies, num_features)))  # (no. movies, no. features)
    Theta = np.matrix(np.reshape(params[num_movies * num_features:], (num_users, num_features)))  # (no. users, no. features)
    
    # initializations
    J = 0
    X_grad = np.zeros(X.shape)  # (no. movies, no. features)
    Theta_grad = np.zeros(Theta.shape)  # (no. users, no. features)
    
    # compute the cost
    error = np.multiply((X * Theta.T) - Y, R)  # error of predicted rating vs actual rating given in Y and R
    squared_error = np.power(error, 2)         # dimensions are (no. movies, no. users)
    J = (1. / 2) * np.sum(squared_error)
    
    # add the cost regularization
    J = J + ((regularization / 2) * np.sum(np.power(Theta, 2)))
    J = J + ((regularization / 2) * np.sum(np.power(X, 2)))
    
    # calculate the gradients with regularization
    X_grad = (error * Theta) + (regularization * X)
    Theta_grad = (error.T * X) + (regularization * Theta)
    
    # unravel the gradient matrices into a single array
    grad = np.concatenate((np.ravel(X_grad), np.ravel(Theta_grad)))
    
    return J, grad              # we return the gradients too in order to obtain better optimization later with scipy.minimize

# Load ratings data:
data = loadmat('ex8_movies.mat')
Y = data['Y']       # Y (no. of movies x no. of users) is the matrix of ratings
R = data['R']       # R (no. of movies x no. of users) is the matrix of indicators whether a movie has been rated by a user
print Y[1,R[1,:]].mean()    # example of average rating of movie no. 2 (in row 1)

movie_idx = {}                  # create a dictionary
f = open('movie_ids.txt')       # open the file that contains the names of the films
for line in f:                  # and save the films in the dictionary, their names accessible via their number
    tokens = line.split(' ')
    tokens[-1] = tokens[-1][:-1]
    movie_idx[int(tokens[0]) - 1] = ' '.join(tokens[1:])
    
print movie_idx[0]

ratings = np.zeros((1682, 1))   # define a blank ratings vector (try also with all ones or twos etc., instead of zeros)
# Insert new user ratings manually (try also different movies and ratings, and more movies):
ratings[0] = 4  
ratings[6] = 3
ratings[11] = 5
ratings[53] = 4
ratings[63] = 5
ratings[65] = 3
ratings[68] = 4
ratings[97] = 2
ratings[182] = 4
ratings[225] = 5
ratings[354] = 5
# Print the new user ratings: 
print('Rated {0} with {1} stars.'.format(movie_idx[0], str(int(ratings[0]))))
print('Rated {0} with {1} stars.'.format(movie_idx[6], str(int(ratings[6]))))
print('Rated {0} with {1} stars.'.format(movie_idx[11], str(int(ratings[11]))))
print('Rated {0} with {1} stars.'.format(movie_idx[53], str(int(ratings[53]))))
print('Rated {0} with {1} stars.'.format(movie_idx[63], str(int(ratings[63]))))
print('Rated {0} with {1} stars.'.format(movie_idx[65], str(int(ratings[65]))))
print('Rated {0} with {1} stars.'.format(movie_idx[68], str(int(ratings[68]))))
print('Rated {0} with {1} stars.'.format(movie_idx[97], str(int(ratings[97]))))
print('Rated {0} with {1} stars.'.format(movie_idx[182], str(int(ratings[182]))))
print('Rated {0} with {1} stars.'.format(movie_idx[225], str(int(ratings[225]))))
print('Rated {0} with {1} stars.'.format(movie_idx[354], str(int(ratings[354]))))
# Add the new ratings in Y and R as a new column: 
Y = np.append(Y, ratings, axis=1)
R = np.append(R, ratings != 0, axis=1)

movies = Y.shape[0]     # no. of movies
users = Y.shape[1]      # no. of users (should be plus one than previous)
features = 10           # select number of features (try different values)
regularization = 1.     # select regularization (try different values)

# Generate x and theta parameters randomly (values between 0 and 1): 
X = np.random.random(size=(movies, features))
Theta = np.random.random(size=(users, features))
params = np.concatenate((np.ravel(X), np.ravel(Theta)))

# Initialize Ymean and Ynorm matrices:
Ymean = np.zeros((movies, 1))
Ynorm = np.zeros((movies, users))

for i in range(movies):                     # For each movie:
    idx = np.where(R[i,:] == 1)[0]          #   find all ratings 
    Ymean[i] = Y[i,idx].mean()              #   find mean rating 
    Ynorm[i,idx] = Y[i,idx] - Ymean[i]      #   normalize the Y matrix

print Ynorm.mean() # should be close to zero     

# Minimize the cost function (try with different num. of iterations):
from scipy.optimize import minimize
fmin = minimize(fun=cost, x0=params, args=(Ynorm, R, features, regularization), 
                method='CG', jac=True, options={'maxiter': 100})        # jac=True becasue the cost function also returns the gradients
print fmin              

X = np.matrix(np.reshape(fmin.x[:movies * features], (movies, features)))       # form the matrix of X parameters
Theta = np.matrix(np.reshape(fmin.x[movies * features:], (users, features)))    # form the matrix of Theta parameters

predictions = X * Theta.T                       # calculate the predictions
my_preds = predictions[:, -1] + Ymean           # add the mean to them

sorted_preds = np.sort(my_preds, axis=0)[::-1]  # sort the predictions (this gives them in ascending order, [::-1] reverses them)
print sorted_preds[:10]                         # print the top 10 predictions

idx = np.argsort(my_preds, axis=0)[::-1]    # find the arguments (movies) corresponding to the top 10 predictions
print idx                                       # print the indexes of the top 10 movies

print("Top 10 movie predictions:\n")            # print the top 10 selected movies and their predicted ratings 
for i in range(10):
    j = int(idx[i])
    print('Predicted rating of {0} for movie {1}.'.format(str(float(my_preds[j])), movie_idx[j])) 
# -*- coding: utf-8 -*-
"""
This code shows a simple example of running PCA (Principal Component Analysis) 
to reduce the dimension of given data. It is done with SVD (Singular Value
Decomposition) in numpy to get the vector of the new plane of projection, and
the data points are plotted before and after compression and reconstruction. 
"""

import numpy as np
from matplotlib import pyplot as plt
from scipy.io import loadmat

def pca(X):
    # normalize the features
    X = (X - X.mean()) / X.std()    
    # compute the covariance matrix
    X = np.matrix(X)
    cov = (X.T * X) / X.shape[0]    
    # perform SVD
    U, S, V = np.linalg.svd(cov)    
    return U, S, V

def project_data(X, U, k):
    # reduce the dimension to k
    U_reduced = U[:,:k]
    # project the data on k-hyperplane
    return np.dot(X, U_reduced)

def recover_data(Z, U, k):
    # recover the data to show how they would look after the compression
    U_reduced = U[:,:k]
    return np.dot(Z, U_reduced.T)

# Load and plot data:
data = loadmat('ex7data1.mat')
print data
X = data['X']
fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(X[:, 0], X[:, 1])
# Compute U:
U, S, V = pca(X)
print 'svd', U, S, V
# Project data to 1-dimensional line:
Z = project_data(X, U, 1)
print 'Z', Z
# Recover and plot data to visualise their post compression state:
X_recovered = recover_data(Z, U, 1)
print 'recovered', X_recovered
fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(X_recovered[:, 0], X_recovered[:, 1]) 
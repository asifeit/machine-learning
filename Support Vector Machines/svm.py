# -*- coding: utf-8 -*-
"""
This code shows an example of using a Support Vector Machine (SVM) without a kernel
to solve a classification problem. The SVM is constructed from the scikit learn 
python module. Two scenarios are tested for different values for the penalty
coefficient C.  
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.io import loadmat
from sklearn import svm

# Import, format, and scatter plot data:
raw_data = loadmat('ex6data1.mat')          # original data file
#raw_data = loadmat('ex6data1_2.mat')       # alternate data file, with a point in the middle between both classes (better example)

data = pd.DataFrame(raw_data['X'], columns=['X1', 'X2'])
data['y'] = raw_data['y']

positive = data[data['y'].isin([1])]
negative = data[data['y'].isin([0])]

print data

fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(positive['X1'], positive['X2'], s=50, marker='x', label='Positive')
ax.scatter(negative['X1'], negative['X2'], s=50, marker='o', label='Negative')
ax.legend() 

# create grid to evaluate model
xx = np.linspace(-1, 5, 30)
yy = np.linspace(1, 5, 30)
YY, XX = np.meshgrid(yy, xx)
xy = np.vstack([XX.ravel(), YY.ravel()]).T

# Create two SVMs, one with C=1 and one with C=100
svc1 = svm.LinearSVC(C=1, loss='hinge', max_iter=1000)
svc1.fit(data[['X1', 'X2']], data['y'])
print svc1.score(data[['X1', 'X2']], data['y'])
Z1 = svc1.decision_function(xy).reshape(XX.shape)

svc2 = svm.LinearSVC(C=100, loss='hinge', max_iter=1000)
svc2.fit(data[['X1', 'X2']], data['y'])
print svc2.score(data[['X1', 'X2']], data['y'])
Z2 = svc2.decision_function(xy).reshape(XX.shape) 

# Plot the classifications and the decision boundaries and margins for both scenarios: 
data['SVM 1 Confidence'] = svc1.decision_function(data[['X1', 'X2']])
fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(data['X1'], data['X2'], s=50, c=data['SVM 1 Confidence'], cmap='seismic')
ax.contour(XX, YY, Z1, colors='k', levels=[-1, 0, 1], alpha=0.5, linestyles=['--', '-', '--'])
ax.set_title('SVM (C='+str(svc1.C)+') Decision Confidence')

data['SVM 2 Confidence'] = svc2.decision_function(data[['X1', 'X2']])
fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(data['X1'], data['X2'], s=50, c=data['SVM 2 Confidence'], cmap='seismic')
ax.contour(XX, YY, Z2, colors='k', levels=[-1, 0, 1], alpha=0.5, linestyles=['--', '-', '--'])
ax.set_title('SVM (C='+str(svc2.C)+') Decision Confidence') 
# -*- coding: utf-8 -*-
"""
This code shows an example of linear regression. It does optimization via gradient
descent. Codes for both single variable (data from ex1data1.txt) and multiple variable
regression (data from ex1data2.txt) are given and executed parallelly. 
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def computeCost(X, y, theta):           # calculate cost function value
    inner = np.power(((X * theta.T) - y), 2)
    return np.sum(inner) / (2 * len(X))

def gradientDescent(X, y, theta, alpha, iters):
    temp = np.matrix(np.zeros(theta.shape))
    parameters = int(theta.ravel().shape[1])
    cost = np.zeros(iters)
    
    for i in range(iters):              # in every iteration
        error = (X * theta.T) - y       # compute the error
        
        for j in range(parameters):     # for every parameter update the value
            term = np.multiply(error, X[:,j])
            temp[0,j] = theta[0,j] - ((alpha / len(X)) * np.sum(term))
            
        theta = temp
        cost[i] = computeCost(X, y, theta)
        
    return theta, cost                  # returns theta vector and history of the cost value


data = pd.read_csv('ex1data1.txt', header=None, names=['Population', 'Profit'])     # open data file for single variable regression
data2 = pd.read_csv('ex1data2.txt', header=None, names=['Size', 'Bedrooms', 'Price'])  # open data file for multiple variable regression
data2 = (data2 - data2.mean()) / data2.std()        # normalize the features in the multiple variable regression 

data.insert(0, 'Ones', 1)       # insert a column of ones, these will be the values of x0
data2.insert(0, 'Ones', 1)      # insert a column of ones, these will be the values of x0

print data.head()
print data.describe()
print data2.head()
print data2.describe()

data.plot(kind='scatter', x='Population', y='Profit', figsize=(12,8))   # plot the data points (only possible for single var)

# set X (training data) and y (target variable)
cols = data.shape[1]
X = data.iloc[:,0:cols-1]               # training data features are everything but the last column
y = data.iloc[:,cols-1:cols]            # training data target values are in the last column
print X.head()
print y.head()

cols2 = data2.shape[1]
X2 = data2.iloc[:,0:cols2-1]             # training data features are everything but the last column
y2 = data2.iloc[:,cols2-1:cols2]            # training data target values are in the last column

X = np.matrix(X.values)
y = np.matrix(y.values)
theta = np.matrix(np.array([0,0]))

X2 = np.matrix(X2.values)
y2 = np.matrix(y2.values)
theta2 = np.matrix(np.array([0,0,0]))

alpha = 0.01                            # learning rate (try changing it)
iters = 1000                            # number of iterations (try changing it)

g, cost = gradientDescent(X, y, theta, alpha, iters)        # do a gradient descent to get the theta vector
print g
print cost
g2, cost2 = gradientDescent(X2, y2, theta2, alpha, iters)        # do a gradient descent to get the theta vector for multivar case (notice how the same function works for both cases)
print g2
print cost2

from numpy.linalg import inv
resenie = inv(X.T*X)*X.T*y          # linear regression with normal equation (single feature)
print resenie                       
resenie2 = inv(X2.T*X2)*X2.T*y2     # linear regression with normal equation (multiple features)
print resenie2

x = np.linspace(data.Population.min(), data.Population.max(), 100)
f = g[0, 0] + (g[0, 1] * x)                                         # this is the linear model for prediction 
x12 = np.linspace(data2.Size.min(), data2.Size.max(), 100)
x22 = np.linspace(data2.Bedrooms.min(), data2.Bedrooms.max(), 100)
f2 = g2[0, 0] + (g2[0, 1] * x12) + (g2[0, 2] * x22)                 # this is the linear model for prediction for the multivar case

fig, ax = plt.subplots(figsize=(12,8))
ax.plot(x, f, 'r', label='Prediction')                              # plot the prediction line
ax.scatter(data.Population, data.Profit, label='Traning Data')      # plot the data points
ax.legend(loc=2)
ax.set_xlabel('Population')
ax.set_ylabel('Profit')
ax.set_title('Predicted Profit vs. Population Size')

fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(data2.Size, data2.Bedrooms, data2.Price, label='Training Data')  # plot the data points
X12,X22 = np.meshgrid(x12,x22)
ax.plot_surface(X12, X22, f2, label='Prediction') # plot the prediction plane
ax.set_xlabel('Size')
ax.set_ylabel('Bedrooms')
ax.set_zlabel('Price')
ax.set_title('Predicted Price vs. House Size')

fig, ax = plt.subplots(figsize=(12,8))
ax.plot(np.arange(iters), cost, 'r')                                # plot the history of the cost value 
ax.set_xlabel('Iterations')
ax.set_ylabel('Cost')
ax.set_title('Error vs. Training Epoch (single feature)')

fig, ax = plt.subplots(figsize=(12,8))
ax.plot(np.arange(iters), cost2, 'r')                               # plot the history of the cost value for the multivar regression
ax.set_xlabel('Iterations')
ax.set_ylabel('Cost')
ax.set_title('Error vs. Training Epoch (two features)') 